<?php

use HMS\Models;

class UserPasswordTest extends TestCase
{

    public function testIsHashedPassword()
    {
        $user = new Models\User();

        $user->password = '123456';

        $this->assertNotEquals('123456', $user->password);
    }

    public function testIsCorrectlyHashedPassword()
    {
        $user = new Models\User();

        $user->password = '123456';

        $this->assertTrue(Hash::check('123456', $user->password));
    }

    public function testPasswordNotSerialized()
    {
        $user = new Models\User();

        $user->password = '123456';

        $json = $user->toJson();
        $this->assertJson($json);

        $array = json_decode($json, true);
        $this->assertArrayNotHasKey('password', $array);
    }

}
