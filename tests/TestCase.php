<?php

abstract class TestCase extends Illuminate\Foundation\Testing\TestCase
{

    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://localhost';

    protected $app;

    public function __construct()
    {
        $this->createApplication();
    }

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $this->app = require __DIR__ . '/../bootstrap/app.php';
        
        $this->app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();
        
        return $this->app;
    }
}
