<?php
use HMS\Manager\UserManager;
use HMS\Models\User;
use Illuminate\Support\Facades\Auth;

/**
 * @group dev
 *
 * @author wetcher
 *        
 */
class UserControllerTest extends TestCase
{

    /*
     * Route::get('/users', 'UserController@getUsers');
     *
     * Route::post('/users', 'UserController@addUser');
     *
     * Route::delete('/users/{id}', 'UserController@deleteUser');
     *
     * Route::get('/users/{id}', 'UserController@getUser');
     *
     * Route::put('/users/{id}', 'UserController@updateUser');
     *
     * Route::post('/users/login', 'UserController@loginUser');
     *
     * Route::post('/users/logout', 'UserController@logoutUser');
     *
     * Route::get('/users/current_user', 'UserController@currentUser');
     */
    
    /** @var UserManager */
    private $userManager;

    public function __construct()
    {
        parent::__construct();
        $this->userManager = $this->app->make(UserManager::class);
    }

    public function testGetOne()
    {
        DB::beginTransaction();
        $testEmail = "mytest@example.com";
        $user = new User();
        $user->email = $testEmail;
        $user->position_id = 1;
        $user->role = "employee";
        $this->userManager->save($user);
        
        $id = $user->id;
        
        $this->json('GET', '/api/users/' . $id)->seeJson([
            'position_id' => $user->position->id,
            'email' => $user->email,
            'role' => $user->role
        ]);
        
        DB::rollBack();
    }

    public function testGetAll()
    {
        DB::beginTransaction();
        
        $this->get('/api/users')->seeJsonStructure([
            'users' => [
                [
                    'id',
                    'email',
                    'full_name',
                    'position' => [
                        'position_name'
                    ]
                ]
            ]
        ]);
        
        DB::rollBack();
    }

    public function testSave()
    {
        DB::beginTransaction();
        $testEmail = "mytest@example.com";
        $user = new User();
        $user->email = $testEmail;
        $user->position_id = 1;
        $user->role = "employee";
        
        $countUsers1 = User::count();
        
        $this->post('/api/users', [
            'email' => $user->email,
            'position_id' => $user->position->id,
            'role' => $user->role
        ]);
        
        $countUsers2 = User::count();
        
        $this->assertNotEquals($countUsers1, $countUsers2, 'user saved');
        
        $userFromDb = $this->userManager->findByEmail($testEmail);
        
        $this->assertTrue(isset($userFromDb), 'user loaded');
        
        $this->assertTrue($user->role == $userFromDb->role, 'user saved correctly');
        
        DB::rollBack();
    }

    public function testUpdate()
    {
        DB::beginTransaction();
        $testEmail = "mytessst@example.com";
        $user = new User();
        $user->email = $testEmail;
        $user->position_id = 1;
        $user->role = "employee";
        
        $this->userManager->save($user);
        
        $testUpdateEmail = 'mytest@example.com';
        $testUpdateFullName = 'Full name';
        $testUpdateFullNameCase = 'Full name case';
        $testUpdatePosition_id = 2;
        $testUpdateAddress = 'address';
        
        $this->assertTrue(! isset($user->full_name), 'data not filled');
        
        $this->put('/api/users/' . $user->id, [
            'email' => $testUpdateEmail
        ]);
        
        $userDb = $this->userManager->get($user->id);
        
        $this->assertFalse($user->email == $userDb->email, 'data changed');
        
        $this->put('/api/users/' . $user->id, [
            'full_name' => $testUpdateFullName,
            'full_name_case' => $testUpdateFullNameCase
        ]);
        
        $userDb = $this->userManager->get($user->id);
        
        $this->assertFalse($userDb->email == $testEmail, 'user email not changed');
        $this->assertTrue($userDb->full_name == $testUpdateFullName, 'user full name changed');
        $this->assertTrue($userDb->full_name_case == $testUpdateFullNameCase, 'user full name case changed');
        
        $testUpdateFullName = 'test full name';
        $this->put('/api/users/' . $user->id, [
            'full_name' => $testUpdateFullName
        ]);
        
        $userDb = $this->userManager->get($user->id);
        
        $this->assertTrue($userDb->full_name == $testUpdateFullName, 'user full name changed');
        $this->assertTrue($userDb->full_name_case == $testUpdateFullNameCase, 'user full name case not changed');
        
        DB::rollBack();
    }

    public function testDelete()
    {
        DB::beginTransaction();
        $testEmail = "mytest@example.com";
        $user = new User();
        $user->email = $testEmail;
        $user->position_id = 1;
        $user->role = "employee";
        
        $this->userManager->save($user);
        
        $this->delete('/api/users/' . $user->id)->seeJson([
            'email' => $user->email,
            'position_id' => $user->position->id,
            'role' => $user->role
        ]);
        
        $usernew = $user->newInstance();
        
        $userDb = $this->userManager->get($user->id);
        
        $this->assertFalse(isset($userDb), 'deleted');
        
        DB::rollBack();
    }

    public function testLoginLogout()
    {
        DB::beginTransaction();
        $email = 'employee@example.com';
        $password = '123';
        
        $user = Auth::user();
        $this->assertFalse(isset($user), 'not login');
        
        $this->post('/api/users/login', [
            'email' => $email,
            'password' => $password
        ])->seeJson([
            'email' => $email
        ]);
        
        $user = Auth::user();
        $this->assertTrue(isset($user), 'login');
        
        $this->get('/api/users/current_user')->seeJson([
            'email' => $email
        ]);
        
        $this->post('/api/users/logout')->seeJson([
            'email' => $email
        ]);
        
        $user = Auth::user();
        $this->assertFalse(isset($user), 'logout complete');
        
        $this->get('/api/users/current_user')->seeJson([
            'user' => null
        ]);
        
        DB::rollBack();
    }
}







