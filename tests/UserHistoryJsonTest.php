<?php

use HMS\Models;

class UserHistoryJsonTest extends TestCase
{

    public function testIsSerialized()
    {
        \DB::beginTransaction();

        $user = Models\User::first();

        $this->assertNotNull($user);

        $record = [
            'old' => 'old_value',
            'new' => 'new_value'
        ];

        $history = new Models\UserChangeHistory();
        $history->change_record = $record;

        $user->changeHistory()->save($history);
        $this->assertNotNull($history->id);

        $history->fresh();

        $this->assertArraySubset($record, $history->change_record);
    }

}
