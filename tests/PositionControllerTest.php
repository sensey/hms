<?php
use HMS\Manager\PositionManager;
use HMS\Models\Position;

/**
 * @group dev
 *
 * @author wetcher
 *        
 */
class PositionControllerTest extends TestCase
{

    /*
     * Route::get('/users', 'UserController@getUsers');
     *
     * Route::post('/users', 'UserController@addUser');
     *
     * Route::delete('/users/{id}', 'UserController@deleteUser');
     *
     * Route::get('/users/{id}', 'UserController@getUser');
     *
     * Route::put('/users/{id}', 'UserController@updateUser');
     *
     * Route::post('/users/login', 'UserController@loginUser');
     *
     * Route::post('/users/logout', 'UserController@logoutUser');
     *
     * Route::get('/users/current_user', 'UserController@currentUser');
     */
    
    /** @var PositionManager */
    private $positionManager;

    public function __construct()
    {
        parent::__construct();
        $this->positionManager = $this->app->make(PositionManager::class);
    }

    public function testGetOne()
    {
        DB::beginTransaction();
        
        $this->get('/api/positions/1')->seeJsonStructure([
            'position' => [
                'id',
                'position_name',
                'position_name_case'
            ]
        ]);
        
        DB::rollBack();
    }

    public function testGetAll()
    {
        DB::beginTransaction();
        
        $this->get('/api/positions')->seeJsonStructure([
            'positions' => [
                [
                    'id',
                    'position_name',
                    'position_name_case'
                ]
            ]
        ]);
        
        DB::rollBack();
    }

    public function testSave()
    {
        DB::beginTransaction();
        
        $position_name = 'position_name';
        $position_name_case = 'position_name_case';
        
        $this->post('/api/positions', [
            'position_name' => $position_name,
            'position_name_case' => $position_name_case
        ])->seeJson([
            'position_name' => $position_name,
            'position_name_case' => $position_name_case
        ]);
        
        $position = $this->positionManager->findByPositionName($position_name);
        $this->assertTrue(isset($position), 'data saved');
        
        DB::rollBack();
    }

    public function testUpdate()
    {
        DB::beginTransaction();
        $position_name = 'position_name';
        $position_name_case = 'position_name_case';
        $position = new Position();
        $position->position_name = $position_name;
        $position->position_name_case = $position_name_case;
        
        $this->positionManager->save($position);
        
        $positionNameUpdate = 'pnosition_name_update';
        $positionNameCaseUpdate = 'position_name_update';
        
        $this->put('/api/positions/' . $position->id, [
            'position_name' => $positionNameUpdate,
            'position_name_case' => $positionNameCaseUpdate
        ]);
        $positionDb = $this->positionManager->get($position->id);
        $this->assertFalse($position->position_name == $positionDb->position_name, 'positon name changed');
        $this->assertFalse($position->position_name_case == $positionDb->position_name_case, 'positon name case changed');
        
        DB::rollBack();
    }

    public function testUpdate1()
    {
        DB::beginTransaction();
        $position_name = 'position_name';
        $position_name_case = 'position_name_case';
        $position = new Position();
        $position->position_name = $position_name;
        $position->position_name_case = $position_name_case;
        
        $this->positionManager->save($position);
        
        $positionNameUpdate = 'pnosition_name_update';
        $positionNameCaseUpdate = 'position_name_update';
        
        $this->put('/api/positions/' . $position->id, [
            'position_name' => $positionNameUpdate
        ]);
        $positionDb = $this->positionManager->get($position->id);
        $this->assertFalse($position->position_name == $positionDb->position_name, 'positon name changed');
        
        $this->put('/api/positions/' . $position->id, [
            'position_name_case' => $positionNameCaseUpdate
        ]);
        $positionDb = $this->positionManager->get($position->id);
        $this->assertFalse($position->position_name_case == $positionDb->position_name_case, 'positon name case changed');
        
        DB::rollBack();
    }

    public function testDelete()
    {
        DB::beginTransaction();
        
        $position_name = 'position_name';
        $position_name_case = 'position_name_case';
        $position = new Position();
        $position->position_name = $position_name;
        $position->position_name_case = $position_name_case;
        
        $this->positionManager->save($position);
        
        $this->delete('/api/positions/' . $position->id)->seeJson([
            'position_name' => $position_name
        ]);
        
        $positionDb = $this->positionManager->get($position->id);
        
        $this->assertFalse(isset($positionDb), 'position deleted');
        
        DB::rollBack();
    }
}







