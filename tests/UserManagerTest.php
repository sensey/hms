<?php
use HMS\Enums\Roles;
use HMS\Manager\PositionManager;
use HMS\Manager\UserManager;
use HMS\Models\User;
use Illuminate\Support\Facades\DB;

/**
 * @group test
 * 
 * @author wetcher
 *        
 */
class UserManagerTest extends TestCase
{

    /** @var UserManager */
    protected $manager;

    /** @var PositionManager */
    protected $positionManager;

    public function __construct()
    {
        parent::__construct();
        $this->manager = $this->app->make(UserManager::class);
        $this->positionManager = $this->app->make(PositionManager::class);
    }

    public function testUserManager()
    {
        DB::beginTransaction();
        $user = new User();
        
        $testEmail = "mytest@example.com";
        
        $user->email = $testEmail;
        $user->position_id = 1;
        $user->role = "employee";
        $this->manager->save($user);
        
        $userdb = User::where("email", $testEmail)->first();
        
        $this->assertTrue(isset($userdb), "saved");
        
        $userdb = $this->manager->findByEmail($testEmail);
        $this->assertTrue(isset($userdb), "find by email is working");
        
        $this->manager->delete($userdb->id);
        $userdb2 = $this->manager->get($userdb->id);
        $this->assertFalse(isset($userdb2), "not deleted");
        DB::rollBack();
    }

    public function testRegister()
    {
        DB::beginTransaction();
        $email = "mytest@example.com";
        $position_id = 1;
        $position = $this->positionManager->get($position_id);
        $role = Roles::EMPLOYEE;
        $this->manager->register($email, $role, $position_id);
        
        $user = $this->manager->findByEmail($email);
        
        $this->assertTrue(isset($user), "loaded");
        $this->assertTrue($user->position->position_name == $position->position_name, "position saved correctly");
        $this->assertTrue($user->email == $email, "email saved correctly");
        $this->assertTrue($user->role == $role, "role saved correctly");
        $this->assertTrue(isset($user->passwordReset), "password reset attached");
        DB::rollBack();
    }

    public function testChangeResetPassword()
    {
        DB::beginTransaction();
        $email = "mytest@example.com";
        $position_id = 1;
        $role = Roles::EMPLOYEE;
        $this->manager->register($email, $role, $position_id);
        
        $user = $this->manager->findByEmail($email);        
        $code1 = $user->passwordReset->code;
        
        $this->manager->changeResetPassword($user);
        $code2 = $user->passwordReset->code;
        
        $this->assertFalse($code1 == $code2, "reset password changed");
        DB::rollBack();
    }
}