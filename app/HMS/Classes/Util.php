<?php
namespace HMS\Classes;

class Util
{

    public static function crc32($value)
    {
        $value = strtolower($value);
        
        return sprintf('%u', crc32($value));
    }
}
