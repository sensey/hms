<?php
namespace HMS\Repository;

use HMS\Models\SystemParameters;

class SystemParametersRepository extends AbstractRepository
{

    public function __construct()
    {
        parent::__construct(SystemParameters::class);
    }

    public function getByName($name)
    {
        return SystemParameters::where('name', $name)->first();
    }
}
