<?php
namespace HMS\Repository;

use HMS\Models\Request;

class RequestRepository extends AbstractRepository
{

    public function __construct()
    {
        parent::__construct(Request::class);
    }
}
