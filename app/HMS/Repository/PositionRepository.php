<?php
namespace HMS\Repository;

use HMS\Models\Position;

class PositionRepository extends AbstractRepository
{

    public function __construct()
    {
        parent::__construct(Position::class);
    }

    public function findByPositionName($positionName)
    {
        return Position::where('position_name', $positionName)->first();
    }
}
