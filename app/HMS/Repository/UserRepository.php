<?php
namespace HMS\Repository;

use HMS\Classes\Util;
use HMS\Models\BaseModel;
use HMS\Models\Position;
use HMS\Models\User;

class UserRepository extends AbstractRepository
{

    public function __construct()
    {
        parent::__construct(User::class);
    }

    public function findByEmail($email)
    {
        $email_crc32 = Util::crc32($email);
        return User::where("email_crc32", $email_crc32)->where("email", $email)->first();
    }

    public function save(BaseModel $entity)
    {
        // даже типы приводить не надо и так работает
        $entity->email_crc32 = Util::crc32($entity->email);
        parent::save($entity);
    }

    public function getAll()
    {
        return User::with('position')->get();
    }

    public function get($id)
    {
        return User::with('position')->find($id);
    }

    public function associatePosition(User $user, Position $position)
    {
        $user->position()->associate($position);
    }
}
