<?php
namespace HMS\Repository;

use HMS\Models\PasswordReset;

class PasswordResetRepository extends AbstractRepository
{

    public function __construct()
    {
        parent::__construct(PasswordReset::class);
    }
}
