<?php
namespace HMS\Models;

class PasswordReset extends BaseModel
{

    protected $table = "password_resets";

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
