<?php
namespace HMS\Models;

class Position extends BaseModel
{

    protected $table = "positions";

    public $timestamps = false;

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
