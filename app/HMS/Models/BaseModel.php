<?php
namespace HMS\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Базовый класс для моделей
 */
abstract class BaseModel extends Model
{
}
