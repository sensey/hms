<?php
namespace HMS\Models;

class SystemParameters extends BaseModel
{

    protected $table = "system_parameters";

    public $timestamps = false;
}
