<?php
namespace HMS\Models;

use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Support\Facades\Hash;

class User extends BaseModel implements Authenticatable
{
    use \Illuminate\Auth\Authenticatable, Authorizable, CanResetPassword;

    protected $table = "users";

    protected $casts = [
        "is_blocked" => "boolean"
    ];

    protected $hidden = [
        "password",
        "remember_token",
        "email_crc32"
    ];

    protected $fillable = [
        "email",
        "position_id",
        "role"
    ];

    public function position()
    {
        return $this->belongsTo(Position::class);
    }

    public function requests()
    {
        return $this->hasMany(Request::class);
    }

    public function changeHistory()
    {
        return $this->hasMany(UserChangeHistory::class);
    }

    public function passwordReset()
    {
        return $this->hasOne(PasswordReset::class);
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }
}
