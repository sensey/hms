<?php
namespace HMS\Models;

use Illuminate\Database\Eloquent\Model;

class RequestHistory extends Model
{

    protected $table = "request_histories";

    protected $casts = [
        "change_record" => "array"
    ];

    public function request()
    {
        $this->belongsTo(Request::class);
    }

    public function user()
    {
        $this->belongsTo(User::class);
    }
}
