<?php
namespace HMS\Models;

class UserChangeHistory extends BaseModel
{

    protected $table = "user_change_histories";
    
    protected $casts = [
        "change_record" => "array"
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
