<?php
namespace HMS\Models;

class SystemParametersHistory extends BaseModel
{

    protected $table = "system_parameters_histories";

    protected $casts = [
        "change_record" => "array"
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
