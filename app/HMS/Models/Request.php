<?php
namespace HMS\Models;

class Request extends BaseModel
{

    protected $table = "requests";

    protected $dates = [
        'start_date',
        'end_date'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
