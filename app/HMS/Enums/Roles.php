<?php
namespace HMS\Enums;

class Roles extends AbstractEnum
{

    const EMPLOYEE = "employee";

    const LEADER = "leader";
}
