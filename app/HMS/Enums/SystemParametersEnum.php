<?php
namespace HMS\Enums;

class SystemParametersEnum extends AbstractEnum
{

    const ORGANISATION_NAME = "organisation_name";

    const DIRECTOR_FULL_NAME = "director_full_name";

    const MAX_HOLIDAY_DURATION = "max_holiday_duration";

    const MIN_HOLIDAY_DURATION = "min_holiday_duration";
}
