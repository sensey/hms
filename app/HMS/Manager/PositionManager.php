<?php
namespace HMS\Manager;

use HMS\Models\Position;
use HMS\Repository\PositionRepository;

class PositionManager extends AbstractManager
{

    public function __construct(PositionRepository $repository)
    {
        parent::__construct($repository);
    }

    public function updatePosition($id, array $updateData)
    {
        $position = $this->get($id);
        $position->position_name = array_get($updateData, 'position_name', $position->position_name);
        $position->position_name_case = array_get($updateData, 'position_name_case', $position->position_name_case);
        $this->update($position);
        
        return $position;
    }

    public function findByPositionName($positionName)
    {
        return $this->repository->findByPositionName($positionName);
    }

    public function delete($id)
    {
        $position = $this->get($id);
        if ($this->canDeletePosition($position)) {
            parent::delete($id);
            return true;
        } {
            return false;
        }
    }
    
    private function canDeletePosition($position)
    {
        return $position->users->count() == 0;
    }
}
