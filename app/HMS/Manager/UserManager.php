<?php
namespace HMS\Manager;

use HMS\Mail\RegistrationMail;
use HMS\Models\Position;
use HMS\Models\User;
use HMS\Repository\UserRepository;
use Illuminate\Support\Facades\Mail;

class UserManager extends AbstractManager
{

    private $passwordResetManager;

    private $positionManager;

    public function __construct(
        UserRepository $repository,
        PasswordResetManager $passwordResetManager,
        PositionManager $positionManager
    ) {
    
        parent::__construct($repository);
        $this->passwordResetManager = $passwordResetManager;
        $this->positionManager = $positionManager;
    }

    public function findByEmail($email)
    {
        return $this->repository->findByEmail($email);
    }

    public function associatePosition(User $user, Position $position)
    {
        $this->repository->associatePosition($user, $position);
    }

    public function register($email, $role, $position_id)
    {
        $position = $this->positionManager->get($position_id);
        
        $user = new User();
        $user->email = $email;
        $user->role = $role;
        $this->associatePosition($user, $position);
        
        $this->save($user);
        $this->changeResetPassword($user);
        
        Mail::to($user)->queue(new RegistrationMail($user));
        
        return $user;
    }

    public function changeResetPassword(User $user)
    {
        $this->passwordResetManager->generatePasswordReset($user);
    }

    public function updateUser($id, array $updateData)
    {
        $user = $this->get($id);
        $user->email = array_get($updateData, 'email', $user->email);
        $user->full_name = array_get($updateData, 'full_name', $user->full_name);
        $user->full_name_case = array_get($updateData, 'full_name_case', $user->full_name_case);
        
        $position_id = array_get($updateData, 'position_id', $user->position->id);
        $position = $this->positionManager->get($position_id);
        $this->associatePosition($user, $position);
        
        $user->address = array_get($updateData, 'address', $user->address);
        
        $this->update($user);
        return $user;
    }
}
