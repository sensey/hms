<?php
namespace HMS\Manager;

use HMS\Repository\SystemParametersRepository;

class SystemParametersManager extends AbstractManager
{

    public function __construct(SystemParametersRepository $repository)
    {
        parent::__construct($repository);
    }

    public function updateSystemParameter($id, $value)
    {
        $systemParameter = $this->repository->get($id);
        $systemParameter->name = $value;
        $this->update($systemParameter);
        
        return $systemParameter;
    }
    
    public function getByName($name)
    {
        return $this->repository->getByName($name);
    }
}
