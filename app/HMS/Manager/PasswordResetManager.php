<?php
namespace HMS\Manager;

use HMS\Models\PasswordReset;
use HMS\Models\User;
use HMS\Repository\PasswordResetRepository;

class PasswordResetManager extends AbstractManager
{

    public function __construct(PasswordResetRepository $repository)
    {
        parent::__construct($repository);
    }

    public function generatePasswordReset(User $user)
    {
        $passwordReset = $user->passwordReset;
        $code = str_random(20);
        if (! isset($passwordReset)) {
            $passwordReset = new PasswordReset();
            $passwordReset->code = $code;
            $user->passwordReset()->save($passwordReset);
        } else {
            $passwordReset->code = $code;
            $this->update($passwordReset);
        }
    }
}
