<?php
namespace HMS\Manager;

use HMS\Models\BaseModel;

abstract class AbstractManager
{

    protected $repository;

    public function __construct($repository)
    {
        $this->repository = $repository;
    }

    public function save(BaseModel $model)
    {
        $this->repository->save($model);
    }
    
    public function update(BaseModel $model)
    {
        $this->repository->update($model);
    }

    public function get($id)
    {
        return $this->repository->get($id);
    }

    public function delete($id)
    {
        $this->repository->delete($id);
    }

    public function getAll()
    {
        return $this->repository->getAll();
    }
}
