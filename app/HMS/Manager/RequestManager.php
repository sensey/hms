<?php
namespace HMS\Manager;

use HMS\Repository\RequestRepository;

class RequestManager extends AbstractManager
{

    public function __construct(RequestRepository $repository)
    {
        parent::__construct($repository);
    }
}
