<?php

namespace HMS\Providers;

use Illuminate\Support\ServiceProvider;

class HMSServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(\HMS\Providers\EventServiceProvider::class);
    }
}
