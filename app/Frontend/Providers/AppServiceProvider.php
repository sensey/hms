<?php
namespace Frontend\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind('path', function () {
            return realpath('app/HMS');
        });
        
        \Request::macro('onlyHas', function (array $keys) {
            $data = [];
            
            foreach ($keys as $key) {
                if ($this->has($key)) {
                    $data[$key] = $this->get($key);
                }
            }
            
            return $data;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if (class_exists(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class)) {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
        
        $this->app->register(\HMS\Providers\HMSServiceProvider::class);
    }
}
