<?php
namespace Frontend\Http\Controllers;

use Frontend\Http\Controllers\Controller;
use HMS\Manager\PositionManager;
use HMS\Manager\UserManager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

    private $positionManager;

    private $userManager;

    public function __construct(PositionManager $positionManager, UserManager $userManager)
    {
        $this->positionManager = $positionManager;
        
        $this->userManager = $userManager;
    }

    public function getUsers()
    {
        $users = $this->userManager->getAll();
        return response()->json([
            'users' => $users
        ]);
    }

    public function addUser(Request $request)
    {
        $email = $request->get('email');
        $position_id = $request->get('position_id');
        $role = $request->get('role');
        
        $user = $this->userManager->register($email, $role, $position_id);
        
        return response()->json([
            'user' => $user
        ]);
    }

    public function deleteUser($id)
    {
        $user = $this->userManager->get($id);
        $this->userManager->delete($id);
        
        return response()->json([
            'user' => $user
        ]);
    }

    public function getUser($id)
    {
        $user = $this->userManager->get($id);
        
        return response()->json([
            'user' => $user
        ]);
    }

    public function updateUser(Request $request, $id)
    {
        $updateData = $request->onlyHas([
            'email',
            'full_name',
            'full_name_case',
            'position_id',
            'address'
        ]);
        
        $user = $this->userManager->updateUser($id, $updateData);
        
        return response()->json([
            'user' => $user
        ]);
    }

    public function loginUser(Request $request)
    {
        $email = $request->get('email');
        $password = $request->get('password');
        $user = null;
        
        if (Auth::attempt([
            'email' => $email,
            'password' => $password
        ])) {
            $user = Auth::user();
        }
        
        return response()->json([
            'user' => $user
        ]);
    }

    public function logoutUser()
    {
        $user = Auth::user();
        Auth::logout();
        return response()->json([
            'msg' => 'OK',
            'user' => $user
        ]);
    }

    public function currentUser()
    {
        $user = Auth::user();
        
        return response()->json([
            'user' => $user
        ]);
    }
}
