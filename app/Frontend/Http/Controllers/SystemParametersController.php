<?php
namespace Frontend\Http\Controllers;

use HMS\Manager\SystemParametersManager;
use Illuminate\Http\Request;

class SystemParametersController extends Controller
{

    private $systemParametersManager;

    public function __construct(SystemParametersManager $systemParametersManager)
    {
        $this->systemParametersManager = $systemParametersManager;
    }

    public function getSystemParameters()
    {
        $users = $this->systemParametersManager->getAll();
        return response()->json([
            'system_parameters' => $users
        ]);
    }

    public function getSystemParameter($id)
    {
        $systemParameter = $this->systemParametersManager->get($id);
        
        return response()->json([
            'system_parameter' => $systemParameter
        ]);
    }

    public function getSystemParameterByName($name)
    {
        $systemParameter = $this->systemParametersManager->getByName($name);
        
        return response()->json([
            'system_parameter' => $systemParameter
        ]);
    }

    public function updateSystemParameter(Request $request, $id)
    {
        $value = $request->get('value');
        $systemParameter = $this->systemParametersManager->updateSystemParameter($id, $value);
        
        return response()->json([
            'user' => $systemParameter
        ]);
    }
}
