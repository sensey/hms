<?php
namespace Frontend\Http\Controllers;

use HMS\Manager\PositionManager;
use HMS\Models\Position;
use Illuminate\Http\Request;

class PositionController extends Controller
{

    /** @var PositionManager */
    private $positionManager;

    public function __construct(PositionManager $positionManager)
    {
        $this->positionManager = $positionManager;
    }

    public function getPositions()
    {
        $positions = $this->positionManager->getAll();
        
        return response()->json([
            'positions' => $positions
        ]);
    }

    public function addPosition(Request $request)
    {
        $position_name = $request->get('position_name');
        $position_name_case = $request->get('position_name_case');
        
        $position = new Position();
        $position->position_name = $position_name;
        $position->position_name_case = $position_name_case;
        
        $this->positionManager->save($position);
        
        return response()->json([
            'position' => $position
        ]);
    }

    public function deletePosition($id)
    {
        $position = $this->positionManager->get($id);
        $deleted = $this->positionManager->delete($id);
        
        return response()->json([
            'deleted' => $deleted,
            'position' => $position
        ]);
    }

    public function getPosition($id)
    {
        $position = $this->positionManager->get($id);
        
        return response()->json([
            'position' => $position
        ]);
    }

    public function updatePosition(Request $request, $id)
    {
        $position_name = $request->get('position_name');
        $position_name_case = $request->get('position_name_case');
        
        $updateData = $request->onlyHas(['position_name', 'position_name_case']);
        
        $position = $this->positionManager->updatePosition($id, $updateData);
        return response()->json([
            'position' => $position
        ]);
    }
}
