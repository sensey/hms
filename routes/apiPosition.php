<?php

/*
 * |--------------------------------------------------------------------------
* | API Routes
* |--------------------------------------------------------------------------
* |
* | Here is where you can register API routes for your application. These
* | routes are loaded by the RouteServiceProvider within a group which
* | is assigned the "api" middleware group. Enjoy building your API!
* |
*/
Route::get('/positions', 'PositionController@getPositions');

Route::post('/positions', 'PositionController@addPosition');

Route::delete('/positions/{id}', 'PositionController@deletePosition');

Route::get('/positions/{id}', 'PositionController@getPosition');

Route::put('/positions/{id}', 'PositionController@updatePosition');




