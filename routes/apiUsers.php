<?php

/*
 * |--------------------------------------------------------------------------
 * | API Routes
 * |--------------------------------------------------------------------------
 * |
 * | Here is where you can register API routes for your application. These
 * | routes are loaded by the RouteServiceProvider within a group which
 * | is assigned the "api" middleware group. Enjoy building your API!
 * |
 */
Route::get('/users', 'UserController@getUsers');

Route::post('/users', 'UserController@addUser');

Route::delete('/users/{id}', 'UserController@deleteUser');

Route::get('/users/{id}', 'UserController@getUser');

Route::put('/users/{id}', 'UserController@updateUser');

Route::post('/users/login', 'UserController@loginUser');

Route::post('/users/logout', 'UserController@logoutUser');

Route::get('/users/current_user', 'UserController@currentUser');



