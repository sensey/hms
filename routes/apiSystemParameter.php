<?php

/*
 * |--------------------------------------------------------------------------
* | API Routes
* |--------------------------------------------------------------------------
* |
* | Here is where you can register API routes for your application. These
* | routes are loaded by the RouteServiceProvider within a group which
* | is assigned the "api" middleware group. Enjoy building your API!
* |
*/
Route::get('/sys_params', 'SystemParametersController@getSystemParameters');

Route::get('/sys_params/{id}', 'SystemParametersController@getSystemParameter');

Route::get('/sys_params/{name}', 'SystemParametersController@getSystemParameterByName');

Route::put('/sys_params/{id}', 'SystemParametersController@updatePosition');




