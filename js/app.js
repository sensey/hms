var app = angular.module('app', ['ui.calendar', 'ui.bootstrap'])
app.controller('user', function($scope,$compile,uiCalendarConfig,dayNames,monthNames,$uibModal) {

	var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
	
	//Выбранное событие
	$scope.clickId;
	
	//События
	$scope.events = [
      {color: 'green', textColor: 'white',id: 0, title: 'В отпуск',start: new Date(y, m, 1),end: new Date(y, m, 2),status:"accept",allDay:true},
      {color: 'red', textColor: 'white', id: 1, title: 'Еще в отпуск',start: new Date(y, m, d - 5),end: new Date(y, m, d - 2),status:"fail",allDay:true},
      {color: 'orange', textColor: 'white', id: 2, title: 'Маленький отпуск',start: new Date(y, m, d - 3, 16, 0),status:"wait",allDay:true},

    ];
	
	//Сохранить событие
	$scope.changeEvent = function(text){
		
		$scope.events[$scope.clickId].title = text;
	}
	
	//Выбор события
	$scope.click = function( date, jsEvent, view){
		console.log(date.start)
        $scope.clickId = date.id;
			$scope.events.forEach(function(item, i, arr) {
			
					if($scope.clickId == item.id)
					{
						$scope.clickId = $scope.events.indexOf(item)
						console.log($scope.clickId)
						return
					}
				
			});
			var modal = $uibModal.open({
				templateUrl: 'templates/modal/' + 'eventChange' + '.html',
				controller: 'modal',
				resolve: {
				events: function () {
				  return [$scope.events[$scope.clickId].start,$scope.events[$scope.clickId].end,$scope.events[$scope.clickId].title, "change"];
				}
			  }
		
    		})
			
			modal.result.then(function(ar){
				
				if(ar[0] == "delete")
					$scope.remove()
				else
				{
					ar[1].setDate(ar[1].getDate()+1)
					
					$scope.events[$scope.clickId].start = ar[0];
					$scope.events[$scope.clickId].end = ar[1];
					$scope.events[$scope.clickId].title = ar[2];
				}
		}, function(){console.log('no')})
			//view.calendar.rerenderEvents();
    };
	
	//Перемещение события
     $scope.move = function(event, delta, revertFunc, jsEvent, ui, view){
		console.log(event.start._d)
		console.log(event.end._d)
		$scope.clickId = event.id;
		$scope.events.forEach(function(item, i, arr) {
			
				console.log(item.title)
				
		});
	   $scope.events[$scope.clickId].start = event.start._d
	   $scope.events[$scope.clickId].end = event.end._d
       $scope.alertMessage = ('Event Droped to make dayDelta ' + delta);
    };
	
	
	//Добавление события
    $scope.addEvent = function() {
		var modal = $uibModal.open({
        templateUrl: 'templates/modal/' + 'eventAdd' + '.html',
        controller: 'modal',
		resolve: {
			events: function () {
			  return $scope.events;
			}
      }
		
    	})
		
		modal.result.then(function(ar){
			ar[1].setDate(ar[1].getDate()+1)
			console.log(ar[1])
			$scope.events.push({
				id:$scope.events.length,
				title: ar[2],
				start: new Date(ar[0]),
				end: new Date(ar[1]),
				className: ['newEvent'],
				color: 'orange', 
				textColor: 'white',
				status:'wait',
				allDay:true
		  });
		}, function(){console.log('no')})
		
		/*if(start < end)
		  $scope.events.push({
			id:$scope.events.length,
			title: text,
			start: new Date(start.getFullYear(), start.getMonth(), start.getDate(),1),
			end: new Date(end.getFullYear(), end.getMonth(), end.getDate(),23),
			className: ['newEvent']
		  });
		else 
			alert("укажите правильный диапозон")*/
    };
	
	//Удаление события
    $scope.remove = function() {
      $scope.events.splice($scope.clickId,1);
    };
    
    $scope.changeTo = 'Hungarian';
    /* event source that pulls from google.com */
    $scope.eventSource = {
            url: "http://www.google.com/calendar/feeds/usa__en%40holiday.calendar.google.com/public/basic",
            className: 'gcal-event',           // an option!
            currentTimezone: 'America/Chicago' // an option!
    };
    
    /* event source that calls a function on every view switch */
    $scope.eventsF = function (start, end, timezone, callback) {
      var s = new Date(start).getTime() / 1000;
      var e = new Date(end).getTime() / 1000;
      var m = new Date(start).getMonth();
      var events = [{title: 'Feed Me ' + m,start: s + (50000),end: s + (100000),allDay: false, className: ['customFeed']}];
      callback(events);
    };

    $scope.calEventsExt = {
       color: '#f00',
       textColor: 'yellow',
       events: [ 
          {type:'party',title: 'Lunch',start: new Date(y, m, d, 12, 0),end: new Date(y, m, d, 14, 0),status: "new"},
          {type:'party',title: 'Lunch 2',start: new Date(y, m, d, 12, 0),end: new Date(y, m, d, 14, 0),allDay: false},
          {type:'party',title: 'Click for Google',start: new Date(y, m, 28),end: new Date(y, m, 29),url: 'http://google.com/'}
        ]
    };


    /* alert on Resize */
    $scope.alertOnResize = function(event, delta, revertFunc, jsEvent, ui, view ){
       $scope.alertMessage = ('Event Resized to make dayDelta ' + delta/60000);
    };
    /* add and removes an event source of choice */
    $scope.addRemoveEventSource = function(sources,source) {
      var canAdd = 0;
      angular.forEach(sources,function(value, key){
        if(sources[key] === source){
          sources.splice(key,1);
          canAdd = 1;
        }
      });
      if(canAdd === 0){
        sources.push(source);
      }
    };
	

	
    /* Change View */
    $scope.changeView = function(view,calendar) {
		$scope.alertMessage = "changeview"
      uiCalendarConfig.calendars[calendar].fullCalendar('changeView',view);
    };
    /* Change View */
    $scope.renderCalender = function(calendar) {
      if(uiCalendarConfig.calendars[calendar]){
        uiCalendarConfig.calendars[calendar].fullCalendar('render');
      }
    };
     /* Render Tooltip */
    
    /* config object */
    $scope.uiConfig = {
      calendar:{
        height: 450,
		lang:'ru',
        editable: true,
        header:{
          left: 'month agendaWeek',
          center: 'title',
          right: 'today prev,next'
        },
        eventClick: $scope.click,
        eventDrop: $scope.move,
        eventResize: $scope.alertOnResize,
        eventRender: $scope.eventRender
      }
    };
	
	$scope.uiConfig.calendar.monthNames = monthNames.full;
    $scope.uiConfig.calendar.monthNamesShort = monthNames.short;
    /* event sources array*/
    $scope.eventSources = [$scope.events, $scope.eventSource, $scope.eventsF];
    //$scope.eventSources2 = [$scope.calEventsExt, $scope.eventsF, $scope.events];

});



app.controller('modal', function ($uibModalInstance, $scope, events) {
  var today = function() {
	$scope.start = new Date();
	$scope.end = new Date();
  };
	
  if(events[3] == "change"){
	  var endDate = events[1].getDate()-1;
	  $scope.start = events[0]
	  $scope.end = new Date(events[1])
	  $scope.end.setDate(endDate)
	  $scope.text = events[2]
  }
	else{
		today()
	}
	

  $scope.add = function () {
    $uibModalInstance.close([$scope.start,$scope.end,$scope.text]);
  };
	
  $scope.change = function () {
    $uibModalInstance.close([$scope.start,$scope.end,$scope.text]);
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };	
	
  $scope.delete = function(){
	  $uibModalInstance.close(["delete"]);
	
  }

  $scope.clear = function() {
    $scope.dt = null;
  };

  $scope.inlineOptions = {
    customClass: getDayClass,
    minDate: new Date(),
    showWeeks: true
  };

  $scope.dateOptions = {
    formatYear: 'yy',
    maxDate: new Date(2020, 5, 22),
    minDate: new Date(),
    startingDay: 1
  };

  $scope.toggleMin = function() {
    $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
    $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
  };

  $scope.toggleMin();

  $scope.open1 = function() {
    $scope.popup1.opened = true;
  };

  $scope.open2 = function() {
    $scope.popup2.opened = true;
  };

  $scope.setDate = function(year, month, day) {
    $scope.dt = new Date(year, month, day);
  };

  $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
  $scope.format = $scope.formats[2];
  $scope.altInputFormats = ['M!/d!/yyyy'];

  $scope.popup1 = {
    opened: false
  };

  $scope.popup2 = {
    opened: false
  };

  var tomorrow = new Date();
  tomorrow.setDate(tomorrow.getDate() + 1);
  var afterTomorrow = new Date();
  afterTomorrow.setDate(tomorrow.getDate() + 1);
  $scope.events = [
    {
      date: tomorrow,
      status: 'full'
    },
    {
      date: afterTomorrow,
      status: 'partially'
    }
  ];

  function getDayClass(data) {
    var date = data.date,
      mode = data.mode;
    if (mode === 'day') {
      var dayToCheck = new Date(date).setHours(0,0,0,0);

      for (var i = 0; i < $scope.events.length; i++) {
        var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

        if (dayToCheck === currentDay) {
          return $scope.events[i].status;
        }
      }
    }

    return '';
  }
});
