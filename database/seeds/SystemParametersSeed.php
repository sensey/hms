<?php
use HMS\Enums\SystemParametersEnum;
use HMS\Models\SystemParameters;
use Illuminate\Database\Seeder;

class SystemParametersSeed extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::beginTransaction();
        
        $this->createParam(SystemParametersEnum::ORGANISATION_NAME, "ООО \"Дженерал Софт\"");
        $this->createParam(SystemParametersEnum::DIRECTOR_FULL_NAME, "Пальчик А. В");
        $this->createParam(SystemParametersEnum::MAX_HOLIDAY_DURATION, 42);
        $this->createParam(SystemParametersEnum::MIN_HOLIDAY_DURATION, 21);
        
        \DB::commit();
    }

    private function createParam($name, $value)
    {
        SystemParameters::create([
            'name' => $name,
            'value' => $value
        ]);
    }
}
