<?php
use HMS\Models\Position;
use Illuminate\Database\Seeder;

class PositionSeed extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::beginTransaction();
        
        Position::create([
            'position_name' => 'инженер-программист',
            'position_name_case' => 'инженера-программиста'
        ]);
        
        Position::create([
            'position_name' => 'директор',
            'position_name_case' => 'директора'
        ]);
        
        \DB::commit();
    }
}
