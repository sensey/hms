<?php
use Faker\Factory;
use HMS\Enums\Roles;
use HMS\Models\User;
use Illuminate\Database\Seeder;
use HMS\Manager\UserManager;

class UserSeed extends Seeder
{

    private $faker;
    
    private $userManager;
    
    public function __construct()
    {
        $this->userManager = App::make(UserManager::class);
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::beginTransaction();
        
        $programer_id = 1;
        $director_id = 2;
        
        $this->faker = Factory::create("ru_RU");
        $this->userManager->save($this->generateUser("employee@example.com", Roles::EMPLOYEE, $programer_id));
        $this->userManager->save($this->generateUser("leader@example.com", Roles::LEADER, $director_id));
        for ($i = 0; $i < 2; $i ++) {
            $user = $this->generateUser();
            $this->userManager->save($user);
            //$user->save();
        }
        
        \DB::commit();
    }

    private function generateUser($email = null, $role = null, $position_id = null)
    {
        $user = new User();
        $user->email = isset($email) ? $email : $this->faker->unique()->email;
        $user->password = "123";
        $user->full_name = $this->faker->name;
        $user->full_name_case = $user->full_name . '\'s';
        $user->address = $this->faker->address;
        $user->position_id = isset($position_id) ? $position_id : rand(1, 2);
        $user->role = isset($role) ? $role : Roles::EMPLOYEE;
        return $user;
    }
}
