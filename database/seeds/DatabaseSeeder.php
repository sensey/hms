<?php
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PositionSeed::class);
        $this->call(SystemParametersSeed::class);
        $this->call(UserSeed::class);
        $this->call(RequestSeed::class);
    }
}
