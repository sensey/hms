<?php
use Faker\Factory;
use HMS\Enums\RequestState;
use HMS\Models\Request;
use HMS\Models\User;
use Illuminate\Database\Seeder;

class RequestSeed extends Seeder
{

    private $count;

    private $faker;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->faker = Factory::create("ru_RU");
        $this->count = User::count();
        
        \DB::beginTransaction();
        
        $employee = User::find(1);
        for ($i = 0; $i < 3; $i ++) {
            $this->generateRequest($employee);
        }
        for ($i = 0; $i < 10; $i ++) {
            $this->generateRequest();
        }
        
        \DB::commit();
    }

    private function generateRequest(User $custom_user = null)
    {
        $request = new Request();
        
        $firstDate = $this->faker->dateTimeBetween("2017-03-01", "2017-06-01");
        $secondDate = $this->faker->dateTimeBetween($firstDate, "2017-06-01");
        $request->start_date = $firstDate;
        $request->end_date = $secondDate;
        $request->comment = $this->faker->paragraph;
        $request->request_state = $this->faker->randomElement(RequestState::getAll());
        
        $user = isset($custom_user) ? $custom_user : User::find(rand(1, $this->count));
        
        $user->requests()->save($request);
        
        return $request;
    }
}
