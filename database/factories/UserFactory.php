<?php

use HMS\Models\User;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(User::class, function (Faker\Generator $faker) {
    $user = new User();
    $user->email = $faker->unique()->email;
    $user->password = "123";
    $user->full_name = $faker->name;
    $user->full_name_case = $user->full_name.'\'s';
    $user->address = $faker->address;
    $user->position_id = rand(1,2);
    $user->role = Roles::EMPLOYEE;
    //$user->role = $faker->randomElement(Roles::getAll());
    //return $user;
    return $user->getAttributes();
});
