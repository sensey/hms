<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Holyday Managment System</title>
    <script src="bower_components/jquery/dist/jquery.js"></script>
    <script src="bower_components/bootstrap/dist/js/bootstrap.js"></script>
    <script src="bower_components/angular/angular.js"></script>
    <script src="bower_components/angular-ui-router/release/angular-ui-router.js"></script>
    <script src="bower_components/angular-resource/angular-resource.js"></script>
    <script src="bower_components/angular-bootstrap/ui-bootstrap.js"></script>
    <script src="bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>
    <script src="bower_components/moment/moment.js"></script>
    <script src="bower_components/chart.js/dist/Chart.bundle.js"></script>
    <script src="bower_components/angular-chart.js/dist/angular-chart.js"></script>
    <script src="bower_components/angular-ui-calendar/src/calendar.js"></script>
    <script src="bower_components/fullcalendar/dist/fullcalendar.js"></script>
    <script src="bower_components/fullcalendar/dist/locale/ru.js"></script>
    <script src="bower_components/fullcalendar/dist/gcal.js"></script>
    <link rel="stylesheet" type="text/css" href="bower_components/font-awesome/css/font-awesome.css"></link>
    <link rel="stylesheet" type="text/css" href="bower_components/fullcalendar/dist/fullcalendar.css">
    <link rel="stylesheet" type="text/css" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css.css">
</head>
<body ng-app="app">
<div class="container">
    <div ui-view="body"></div>
    <div ui-view="workzone"></div>
</div>



<!--modules-->
<script src="frontend/app.js"></script>
<script src="frontend/modules/ui.module.js"></script>
<script src="frontend/modules/rout.module.js"></script>
<script src="frontend/modules/data.module.js"></script>
<script src="frontend/modules/resource.module.js"></script>
<!--services-->
<script src="frontend/services/person.service.js"></script>
<script src="frontend/services/list.service.js"></script>
<script src="frontend/services/tableHoliday.service.js"></script>
<script src="frontend/services/options.service.js"></script>
<script src="frontend/services/holidays.service.js"></script>
<!--controllers-->
<script src="frontend/app.controller.js"></script>
<script src="frontend/components/account/dataChangeController.js"></script>
<script src="frontend/components/account/passChangeController.js"></script>
<script src="frontend/components/p_dashboard/modal.js"></script>
<!--resources-->
<script src="frontend/services/resources/statistic.js"></script>
<script src="frontend/services/resources/user.js"></script>
<script src="frontend/services/resources/list.js"></script>
<script src="frontend/services/resources/options.js"></script>
<script src="frontend/services/resources/holidays.js"></script>

<!--components-->
<script src="frontend/components/recovery/recovery.js"></script>
<script src="frontend/components/login/login.js"></script>
<script src="frontend/components/bar/bar.js"></script>
<script src="frontend/components/account/account.js"></script>
<script src="frontend/components/p_statistic/p_statistic.js"></script>
<script src="frontend/components/p_dashboard/p_dashboard.js"></script>
<script src="frontend/components/a_dashboard/a_dashboard.js"></script>
<script src="frontend/components/a_statistic/a_statistic.js"></script>
<script src="frontend/components/administration/administration.js"></script>
<script src="frontend/components/filling/filling.js"></script>
<script src="frontend/components/pass/pass.js"></script>
<!--rout_config-->
<script src="frontend/route.config/route.config.js"></script>
<script src="frontend/components/table_pagination/js/index.js"></script>

<link rel='stylesheet prefetch' href='frontend/ng-table.min.css'>
<script src='frontend/ng-table.min.js'></script>
</body>
</html>