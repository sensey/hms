<!DOCTYPE html>
<html lang="en">
<head>
    <title>@yield('title', 'undefined')</title>

</head>
<body>

    <div style="float: left; width: 20%;">
        @section('sidebar')
            <p>sides</p>
        @show
    </div>
    
    <div class="container" style="float: left; width: 79%;"> 
        @yield('content')
    </div>
</body>
</html>
