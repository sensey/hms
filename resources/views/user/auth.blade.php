@extends('layouts.app')

@section('title', 'auth')


@section('sidebar')
	@parent
	<p>попытка зарегистрировать нового пользователя</p>
@stop

@section('content')
    
	 <form method="POST" action="{{ route('register') }}" >
	 	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	 	
	 	<p>email:</p><input type="text" id="email" name="email" value="asd@asda.asd"/> <br>
	 	
	 	<p>position:</p>
	 	<select name="position_id">
	 		@foreach ($allPositions as $position)
	 			<option label="{{$position->position_name}}" value="{{$position->id}}" />
	 		@endforeach
	 	</select>
	 	<br>
	 	
	 	
	 	<p>role:</p>
	 	<select name="role">
	 		@foreach ($allRoles as $key => $role)
	 			<option label="{{$key}}" value="{{$role}}" />
	 		@endforeach
	 	</select>
	 	<br>
	 	
	 	<input type="submit" value="send"/>
	 	
	 </form>

@endsection