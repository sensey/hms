(function () {
    angular.module('rout.module')
        .config(function ($stateProvider,$urlRouterProvider) {
            $urlRouterProvider.otherwise('/account');
            $stateProvider
                .state('account', {
                        url: '/account',
                        template: '<account></account>'
                    }
                )
                .state('p_statistic',{
                    url: '/p_statistic',
                    template:'<person-statistic></person-statistic>'
                })
                .state('p_dashboard',{
                    url: '/p_dashboard',
                    template:'<person-dashboard></person-dashboard>'
                })
                .state('a_dashboard',{
                    url:'/a_dashboard',
                    template:'<admin-dashboard></admin-dashboard>'
                })
                .state('a_statistic',{
                    url:'/a_statistic',
                    template:'<admin-statistic></admin-statistic>'
                })
                .state('administration',{
                    url:'/administration',
                    template:'<administration></administration>'
                })
                .state('table_pagination',{
                    url:'/table_pagination',
                    template:'<table-pagination></table-pagination>'
                })
        })
})();
