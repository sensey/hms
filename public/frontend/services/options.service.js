/**
 * Created by alexander on 15.2.17.
 */
(function () {
    angular.module('data.module')
        .service('optionsService', OptionsService);

    function OptionsService() {
        this.options = {};

        this.get = function () {
            return this.options;
        };

        this.set = function (options) {
            this.options = options;
        }

    }
})();
