(function () {
    angular.module('data.module')
        .service('personService', PersonService);

    function PersonService() {
        this.person = {};

        this.get = function () {
            return this.person;
        };

        this.set = function (person) {
            this.person = person;
        }

    }
})();