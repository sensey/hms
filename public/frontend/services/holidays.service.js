/**
 * Created by alexander on 16.2.17.
 */
(function () {
    angular.module('data.module')
        .service('holidaysService', HolidaysService);


    function HolidaysService(WAITING_FOR_RESPONSE, ACCEPTED, REJECTED) {

        this.holidays = [];

        this.wait = WAITING_FOR_RESPONSE
        this.accept = ACCEPTED
        this.reject = REJECTED

        this.get = function () {
            return this.holidays;
        };

        this.set = function (holidays) {
            this.holidays = holidays;
        }

        this.add = function(obj){
            this.holidays.push(obj)
        }

    }
})();
