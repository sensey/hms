(function () {
    angular.module('resource.module')
        .factory('List', ['$resource', function ($resource) {
            return $resource('api/list/list.json', {}, {
                list:{
                    method:'GET',
                    isArray: false
                }
            })
        }])

})();