(function () {
    angular.module('resource.module')
        .factory('Statistic', ['$resource', function ($resource) {
            return $resource('api/user//:id.json', {
              id:"@id"
            }, {
                user: {
                    method: 'GET',
                    isArray: false
                }
            })
        }])

})();