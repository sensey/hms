(function () {
    angular.module('resource.module')
        .factory('User', ['$resource', function ($resource) {
            return $resource('api/user/:id.json', {id: "@id"}, {
                user: {
                    method: 'GET',
                    isArray: false
                }
            })
        }])

})();