/**
 * Created by alexander on 15.2.17.
 */
(function () {
    angular.module('resource.module')
        .factory('Options', ['$resource', function ($resource) {
            return $resource('api/options/options.json', {}, {
                options:{
                    method:'GET',
                    isArray: false
                },
                optionsPost:{
                    method:'POST'
                }
            })
        }])

})();