/**
 * Created by alexander on 16.2.17.
 */
(function () {
    angular.module('resource.module')
        .factory('Holidays', ['$resource', function ($resource) {
            return $resource('api/holidays/holidays.json', {}, {
                all:{
                    method:'GET',
                    isArray: true
                }
            })
        }])

})();
