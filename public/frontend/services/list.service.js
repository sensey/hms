(function () {
    angular.module('data.module')
        .service('listService', LsitService);

    function LsitService() {
        this.list = {};

        this.get = function () {
            return this.list;
        };

        this.set = function (list) {
            this.list = list;
        }

    }
})();