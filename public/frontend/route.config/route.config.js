(function () {
    angular.module('rout.module')
        .config(function ($stateProvider, $urlRouterProvider) {
            $urlRouterProvider.otherwise('/login');
            $stateProvider
                .state('account', {
                    url: '/account',
                    views: {
                        'workzone': {
                            template: '<account></account>'
                        },
                        'body': {
                            template: '<bar></bar>'
                        }
                    }
                }).state('p_statistic', {
                url: '/p_statistic',
                views: {
                    'workzone': {
                        template: '<person-statistic></person-statistic>'
                    },
                    'body': {
                        template: '<bar></bar>'
                    }
                }
            }).state('p_dashboard', {
                url: '/p_dashboard',
                views: {
                    'workzone': {
                        template: '<person-dashboard></person-dashboard>'
                    },
                    'body': {
                        template: '<bar></bar>'
                    }
                }
            }).state('a_dashboard', {
                url: '/a_dashboard',
                views: {
                    'workzone': {
                        template: '<admin-dashboard></admin-dashboard>'
                    },
                    'body': {
                        template: '<bar></bar>'
                    }
                }
            }).state('a_statistic', {
                url: '/a_statistic',
                views: {
                    'workzone': {
                        template: '<admin-statistic></admin-statistic>'
                    },
                    'body': {
                        template: '<bar></bar>'
                    }
                }
            }).state('administration', {
                url: '/administration',
                views: {
                    'workzone': {
                        template: '<administration></administration>'
                    },
                    'body': {
                        template: '<bar></bar>'
                    }
                }
            }).state('login', {
                url: '/login',
                views: {
                    'body': {
                        template: '<login></login>'
                    }
                }
            }).state('recovery',{
                url:'/recovery',
                views:{
                    'body':{
                        template:'<recovery></recovery>'
                    }
                }
            }).state('filling',{
                url:'/filling',
                views:{
                    'body':{
                        template:'<filling></filling>'
                    }
                }
            }).state('pass',{
                url:'/pass',
                views:{
                    'body':{
                        template:'<pass></pass>'
                    }
                }
            })
        })
})();
