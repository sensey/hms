(function () {
    angular.module('data.module',[]);
})();

(function () {
    angular.module('data.module')
        .constant("WAITING_FOR_RESPONSE", "waiting_for_response")
        .constant("ACCEPTED", "accepted")
        .constant("REJECTED", "rejected")
})();
