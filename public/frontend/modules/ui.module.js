(function () {
    angular.module('ui.module',[
        'rout.module',
        'data.module',
        'ui.bootstrap',
        'chart.js',
        'resource.module',
        'ui.calendar',
        'ngTable'
    ]);
})();