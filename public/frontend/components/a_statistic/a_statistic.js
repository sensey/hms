(function () {
    angular.module('ui.module')
        .component('adminStatistic',{
            templateUrl:'frontend/components/a_statistic/template.html',
            controller:AdministratorStatisticController,
            bindings:{
                selected:'<'
}
        });
    AdministratorStatisticController.$inject = ['Statistic','List','listService'];
    function AdministratorStatisticController(Statistic,List,listService) {
        var ctrl = this;
        List.list(function (res) {
            ctrl.list= res.workers;
            listService.set(ctrl.list);
        });
        ctrl.selected = '0';
        ctrl.labels = ["Использовано", "Не использовано"];
        ctrl.colors = ['#96FF8A', '#D83119'];
        ctrl.request = function (value) {
           ctrl.statistic = Statistic.user({id:value});
        };
         ctrl.request("0");
    }

})();