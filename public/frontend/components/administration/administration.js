(function () {
    angular.module('ui.module')
        .component('administration',{
            templateUrl:'frontend/components/administration/template.html',
            controller:adminController
        })
    adminController.$inject = ['optionsService', 'Options'];

    function adminController(optionsService, Options){
        var ctrl = this;
        ctrl.service = optionsService;


        Options.options(function (res) {
            ctrl.service.set(res)
            console.log(ctrl.service.get())
        })


    }
})();