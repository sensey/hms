(function () {
    angular.module('ui.module')
        .component('personStatistic', {
            templateUrl: 'frontend/components/p_statistic/template.html',
            controller: PersonStatisticController
        });

    PersonStatisticController.$inject = ['Statistic','personService'];

    function PersonStatisticController(Statistic,personService) {
        var ctrl = this;
        ctrl.diagramDisplay = false;
        ctrl.labels = ["Использовано", "Не использовано"];
        ctrl.colors = ['#96FF8A', '#D83119'];
        var pos = personService.get().id;
        ctrl.data = Statistic.user({id:pos});


        ctrl.changeDisplay = function () {
            ctrl.diagramDisplay = !ctrl.diagramDisplay;
        };

    }

})();