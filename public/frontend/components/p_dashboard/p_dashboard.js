(function () {
    angular.module('ui.module')
        .component('personDashboard', {
            templateUrl: 'frontend/components/p_dashboard/template.html',
            controller: PersonalDashboardController
        });

    function PersonalDashboardController($uibModal, holidaysService) {

        var ctrl = this;
        ctrl.dayNames = {full:["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"],
            short:["Вск", "Пн", "Вт", "Ср", "Чет", "Пят", "Суб"]};
        ctrl.monthNames = {full:["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
            short:["Янв", "Фев", "Март", "Апр", "Май", "Июнь", "Июль", "Авг", "Сент", "Окт", "Нояб", "Дек"]}
        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();

        //Выбранное событие
        ctrl.clickId;

        //События
        ctrl.events = [
            {
                color: 'green',
                textColor: 'white',
                id: 0,
                title: 'В отпуск',
                start: new Date(y, m, 1),
                end: new Date(y, m, 2),
                status: "accept",
                allDay: true
            },
            {
                color: 'red',
                textColor: 'white',
                id: 1,
                title: 'Еще в отпуск',
                start: new Date(y, m, d - 5),
                end: new Date(y, m, d - 2),
                status: "fail",
                allDay: true
            },
            {
                color: 'orange',
                textColor: 'white',
                id: 2,
                title: 'Маленький отпуск',
                start: new Date(y, m, d - 3, 16, 0),
                end: new Date(y, m, d - 1, 16, 0),
                status: "wait",
                allDay: true
            },

        ];

        //Сохранить событие
        ctrl.changeEvent = function (text) {

            ctrl.events[ctrl.clickId].title = text;
        }

        //Выбор события
        ctrl.click = function (date, jsEvent, view) {


            ctrl.clickId = date.id;
            ctrl.events.forEach(function (item, i, arr) {

                if (ctrl.clickId == item.id) {
                    ctrl.clickId = ctrl.events.indexOf(item);
                    return;
                }

            });
            var modal = $uibModal.open({
                templateUrl: "app/components/p_dashboard/eventChange.html",
                controller: 'modal',
                resolve: {
                    events: function () {
                        return [ctrl.events[ctrl.clickId].start, ctrl.events[ctrl.clickId].end, ctrl.events[ctrl.clickId].title, "change"];
                    }
                }

            });

            modal.result.then(function (ar) {

                if (ar[0] == "delete")
                    ctrl.remove()
                else {
                    ar[1].setDate(ar[1].getDate() + 1)

                    ctrl.events[ctrl.clickId].start = ar[0];
                    ctrl.events[ctrl.clickId].end = ar[1];
                    ctrl.events[ctrl.clickId].title = ar[2];
                }
            })
        };

        //Перемещение события
        ctrl.move = function (event, delta, revertFunc, jsEvent, ui, view) {
            ctrl.clickId
            var i=0
            ctrl.events.forEach(function(el){
                if(el.id == event.id)
                    ctrl.clickId = i
                i++
            })
            console.log(ctrl.clickId)
            ctrl.events[ctrl.clickId].start = event.start._d;
            ctrl.events[ctrl.clickId].end = event.end._d;
            ctrl.alertMessage = ('Event Droped to make dayDelta ' + delta);

        };


        //Добавление события
        ctrl.addEvent = function () {
            var modal = $uibModal.open({
                templateUrl: 'app/components/p_dashboard/eventAdd.html',
                controller: 'modal',
                resolve: {
                    events: function () {
                        return ctrl.events;
                    }
                }

            })



            modal.result.then(function (ar) {
                ar[1].setDate(ar[1].getDate() + 1)
                //console.log(ctrl.events.length)

                console.log(ar[1])

                holidaysService.add({
                    id:(ctrl.events.length == 0) ? 0 : ctrl.events[ctrl.events.length - 1].id+1,
                    title: ar[2],
                    name: "cur user",
                    send: new Date(),
                    start: new Date(ar[0]),
                    end: new Date(ar[1]),
                    status: "wait",
                    textColor:'white',
                    color: 'orange',
                    allDay: true
                })
                console.log(holidaysService.get().length)

                ctrl.events.push(holidaysService.get()[holidaysService.get().length - 1]);

            })
        };

        //Удаление события
        ctrl.remove = function () {
            ctrl.events.splice(ctrl.clickId, 1);
        };

        ctrl.changeTo = 'Hungarian';
        /* event source that pulls from google.com */
        ctrl.eventSource = {
            url: "http://www.google.com/calendar/feeds/usa__en%40holiday.calendar.google.com/basic",
            className: 'gcal-event',           // an option!
            currentTimezone: 'America/Chicago' // an option!
        };

        /* event source that calls a function on every view switch */
        ctrl.eventsF = function (start, end, timezone, callback) {
            var s = new Date(start).getTime() / 1000;
            var e = new Date(end).getTime() / 1000;
            var m = new Date(start).getMonth();
            var events = [{
                title: 'Feed Me ' + m,
                start: s + (50000),
                end: s + (100000),
                allDay: false,
                className: ['customFeed']
            }];
            callback(events);
        };



        /* Change View */
        ctrl.changeView = function (view, calendar) {
            ctrl.alertMessage = "changeview";
            uiCalendarConfig.calendars[calendar].fullCalendar('changeView', view);
        };
        /* Change View */
        ctrl.renderCalender = function (calendar) {
            if (uiCalendarConfig.calendars[calendar]) {
                uiCalendarConfig.calendars[calendar].fullCalendar('render');
            }
        };
        /* Render Tooltip */
        /* config object */
        ctrl.uiConfig = {
            calendar: {
                height: 450,
                lang: 'ru',
                editable: true,
                header: {
                    left: 'month agendaWeek',
                    center: 'title',
                    right: 'today prev,next'
                },
                eventClick: ctrl.click,
                eventDrop: ctrl.move,
                eventRender: ctrl.eventRender
            }
        };

        ctrl.uiConfig.calendar.monthNames = ctrl.monthNames.full;
        ctrl.uiConfig.calendar.monthNamesShort = ctrl.monthNames.short;
        ctrl.eventSources = [ctrl.events, ctrl.eventSource, ctrl.eventsF];

    }
})();