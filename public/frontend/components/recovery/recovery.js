(function () {
    angular.module('ui.module')
        .component('recovery',{
            templateUrl:'frontend/components/recovery/template.html',
            controller: function () {
                this.alert = false;
            }
        })
})();