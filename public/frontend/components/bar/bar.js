(function () {
    angular.module('ui.module')
        .component('bar',{
            templateUrl: "frontend/components/bar/template.html",
            controller: BarController
        });
    BarController.$inject = ['User','personService'];
    function BarController(User,personService) {
        var ctrl = this;
        ctrl.position = null;
        ctrl.service = personService;

        ctrl.user = User.user({id:2},function (res) {
            personService.set(res);
            ctrl.position = res.position;
        })


    }
})();