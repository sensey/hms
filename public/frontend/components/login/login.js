(function () {
    angular.module('ui.module')
        .component('login',{
            templateUrl:"frontend/components/login/template.html",
            controller: LoginController
        });

    LoginController.$inject = ['$state','$timeout'];
    function LoginController($state,$timeout) {
        this.icon = true;
        this.login = function () {
            this.icon=false;
            $timeout(function () {
                $state.go('p_dashboard');
            },3000);
        }
    }
})();