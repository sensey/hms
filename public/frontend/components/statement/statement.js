(function () {
	angular.module('ui.module')
		.component('statement',{
			templateUrl:'frontend/components/statement/template.html',
			controller : StatementController
		});
	StatementController.$inject = ['personService'];
	function StatementController(personService) {
		"use strict";
		this.day = new Date().getDate();
		this.month = new Date().getMonth();
		this.year = new Date().getFullYear();
		this.person = personService.get();

	}
})();