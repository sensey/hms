(function () {
    angular.module('ui.module')
        .controller('DataChangeController', function ($scope,$uibModalInstance,data) {
            $scope.data = data;
            $scope.ok = function () {
                $uibModalInstance.close($scope.data);
            };
            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            }
        })
})();