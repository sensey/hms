(function () {
    angular.module('ui.module')
        .component('account', {
            templateUrl: 'frontend/components/account/template.html',
            controller: AccountController
        });
    AccountController.$inject = ['personService','$uibModal'];
    function AccountController(personService,$uibModal) {
        var ctrl = this;
        ctrl.data = personService.get();

        ctrl.dataChange = function () {
            var dataWindow = $uibModal.open({
                templateUrl: 'frontend/components/account/dataChange.html',
                controller: 'DataChangeController',
                resolve: {
                    data:function () {
                       return angular.copy(ctrl.data);
                    }
                }
            });

            dataWindow.result.then(function (res) {
                ctrl.data = res;
                personService.set(res);
            },function (res) {})
        };
        ctrl.passChange = function () {
            var passWindow = $uibModal.open({
                templateUrl: 'frontend/components/account/passChange.html',
                controller: 'PassChangeController'
            });

            passWindow.result.then(function (res) {},function (res) {})
        };


    }

})();