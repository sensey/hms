(function () {
    angular.module('ui.module')
        .controller('PassChangeController', function ($scope,$uibModalInstance) {
            $scope.data={};
            $scope.ok = function () {
                $uibModalInstance.close();
            };
            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            }
        })
})();