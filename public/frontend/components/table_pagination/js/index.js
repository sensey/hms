(function() {
	angular.module('ui.module')
        .component('tablePagination', {
            templateUrl: 'frontend/components/table_pagination/template.html',
            controller: tableController,
			controllerAs:'table'
        });


  function tableController(NgTableParams, holidays, $uibModal, Holidays, holidaysService, $scope) {

  	Holidays.all(function(res){
  		holidaysService.set(res)
		$scope.table = holidaysService.get()
	})

	  $scope.toDateString = function(str){
  			var d = new Date(Date.parse(str))
		  	return d.getDate() + '.' + (d.getMonth() + 1) + '.' + d.getFullYear()
	  }


      $scope.endToDateString = function(str){
          var d = new Date(Date.parse(str))
          return (d.getDate() - 1) + '.' + (d.getMonth() + 1) + '.' + d.getFullYear()
      }

      this.tableParams = new NgTableParams({
          page: 1, // show first page
          count: 5 // count per page
      }, {
          total: 0
      });

	  $scope.colorStyle = function(row){
		  switch(row.status){
			  case "wait":return {"background-color":"#f0ad4e"};
			  case "fail":return {"background-color":"#d9534f"};
			  case "success":return {"background-color":"#5cb85c"};
		  }	  
	  }
	  
	  $scope.params = ["ФИО","Дата подачи","Дата начала","Дата окончания","Действия",]
	  
	  $scope.showButton = function(row){
	  	if(row.status == "wait")
			return true;
		  else return false
	  }
	  
	  $scope.showEye = function(row){
		  if(row.status == "fail")
			  return true
		  else return false
	  }
		
	  $scope.getClass = function(row){
		  switch(row.status){
			  case "wait":return "warning";
			  case "fail":return "danger";
			  case "success":return "success";
		  }	  
	  }
	  
	  $scope.ok = function(row){
		  row.status = "success"
		  console.log(row)
	  }
	  
	  $scope.cancel = function(row){
		  var modal = $uibModal.open({
        	  templateUrl: "app/components/table_pagination/modal/cancel.html",
			  controller: 'cancelModal',
			  size: 'sm'
    	  })
		  
		  modal.result.then(function(text){
			console.log(text)
			row.status = "fail"
			row.more = text
		}, function(){console.log('Отменено')})
		  
	  }

	  

	 	  
  }
})();



angular.module('ui.module').controller('cancelModal', function ($uibModalInstance, $scope) {
	$scope.ok = function(){$uibModalInstance.close($scope.text);}
	$scope.cancel = function(){$uibModalInstance.dismiss('cancel');}
})